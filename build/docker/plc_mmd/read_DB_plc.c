/*
 
 **********************************************************************
 * WARNING: This and other test programs overwrite data in your PLC.  *
 * DO NOT use it on PLC's when anything is connected to their outputs.*
 * This is alpha software. Use entirely on your own risk.             * 
 **********************************************************************
 
 (C) Tono Riesco (tono.riesco@cern.ch) 2013.

 This is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libnodave; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.  
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include "nodavesimple.h"
#include "openSocket.h"

const char *byte_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';

    int z;
    for (z = 128; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}
int main(int argc, char **argv) {
    int a, res, i; 
	int bytes_to_read=1;
	int DBnum=1; 
	int start_position=0;
    //float d;
    daveInterface * di;
    daveConnection * dc;
    _daveOSserialType fds;
    
    daveSetDebug(daveDebugPrintErrors);
    
    if (argc<5) {
	printf("Usage: read_DB_plc [IP-Address of PLC] [Bytes to read] [DB] [Start position]\n");
	exit(-1);
    }    
    sscanf(argv[2],"%d", &bytes_to_read);
    sscanf(argv[3],"%d", &DBnum);
	sscanf(argv[4],"%d", &start_position);

    fds.rfd=openSocket(102, argv[1]);
    fds.wfd=fds.rfd;
    
    if (fds.rfd>0) { 
	di =daveNewInterface(fds,"IF1",0, daveProtoISOTCP, daveSpeed187k);
	daveSetTimeout(di,5000000);
	dc =daveNewConnection(di,2,0, 2);  // insert your rack and slot here
	
	if (0==daveConnectPLC(dc)) {
	    printf("Connected.\n");
		
/*
Explanation of function daveReadBytes(dc,area, DBnum, start, readLen, pbuf);
How to read a bit from I0.2: daveReadBits($dc, daveInputs, 0, 2);
*/
        //printf("\nTrying to read a single bit from E0.2 (supposed to work)\n");
	    //res=daveReadBits(dc, daveInputs, 0, 2, 1,NULL);
	    //printf("function result:%d=%s\n", res, daveStrerror(res));
		//bytes_to_read = 32;
		// Possition, Input, DB, etc to be read
		//start_position = 0;
		//DBnum = 0;
		
	res=daveReadBytes(dc,daveDB, DBnum, start_position, bytes_to_read, NULL);
	if (0==res) { 
		for (i=0;i < bytes_to_read;i++){	
			//a=daveSwapIed_32(a);
    	    a=daveGetU8(dc);
	    printf("Byte %02d: %03d In Binary: %s\n",i,a, byte_to_binary(a));
		} 
		}  else printf("failed! %d=%s\n",res, daveStrerror (res));  

	printf("Finished.\n");
	return 0;
	} else {
	    printf("Couldn't connect to PLC.\n");	
	    return -2;
	}
    } else {
	printf("Couldn't open TCP port. \nPlease make sure a CP is connected and the IP address is ok. \n");	
    	return -1;
    }    
}

/*
    Changes: 
    05/06/13  Tono did this simplified version.
*/
